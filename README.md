EV3BasicExamples
================

Cybercamp example code, basic functions for our robot.

Setup:
------

* Copy files from repository and open project in netbeans
* Build project
* Copy jar file from dist folder to robot:
    scp dist/EV3BasicExamples.jar root@10.0.1.1:/home/lejos/programs/.
* Launch the program on the robot using the LeJOS menu.
* Have fun!